package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Main extends JFrame implements MouseListener {

    private static final long serialVersionUID = 1L;

    public static void main(String[] args) throws ClassNotFoundException,
            InstantiationException, IllegalAccessException,
            UnsupportedLookAndFeelException {

        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        new Main();

    }

    public boolean play = true;
    int[][][] blokkering = new int[2][1000][2];
    private Panel p;

    public MP3 mp3 = new MP3();

    private boolean playing = false;

    private Main() {
        final Timer t2 = new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                playSound();
            }
        });
        t2.setDelay(270000);
        t2.start();
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        JMenu sound = new JMenu("Sound");
        JMenuItem toggle = new JMenuItem("Turn Off/On");
        toggle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (playing) {
                    t2.stop();
                    playSound();
                } else {
                    t2.start();
                }
            }
        });
        JMenuItem menuItem = new JMenuItem("Restart");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < 1000; i++) {
                    blokkering[0][i][0] = (int) (Math.random() * 20);
                    blokkering[1][i][0] = (int) (Math.random() * 20);
                    blokkering[0][i][1] = 0;
                    blokkering[1][i][1] = 0;
                }
                for (int i = 0; i < 100; i++) {
                    blokkering[0][(int) (Math.random() * 980)][1] = 1;
                    blokkering[1][(int) (Math.random() * 980)][1] = 1;
                }
                p.copter = new Copter();
                p.points = 0;
                p.movement = 0;
                p.level1 = 0;
                p.level2 = 0;
                p.setBlokkering(blokkering);
                play = true;
            }
        });
        menu.add(menuItem);
        sound.add(toggle);
        menuBar.add(menu);
        menuBar.add(sound);
        setJMenuBar(menuBar);

        for (int i = 0; i < 1000; i++) {
            blokkering[0][i][0] = (int) (Math.random() * 20);
            blokkering[1][i][0] = (int) (Math.random() * 20);
        }
        for (int i = 0; i < 100; i++) {
            blokkering[0][(int) (Math.random() * 980)][1] = 1;
            blokkering[1][(int) (Math.random() * 980)][1] = 1;
        }

        p = new Panel(blokkering, this);

        setContentPane(p);

        Timer t = new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (play) {
                    p.tick();
                }
            }
        });
        t.setDelay(25);
        t.start();

        setLocation(400, 200);
        setSize(600, 400);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Blokkokopter v2.2 - Released 15th March, 2012");
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        p.copter.falling = false;
        p.copter.howLong = 1;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        p.copter.falling = true;
        p.copter.howLong = 1;
    }

    private void playSound() {
        if (!playing) {
            mp3.play();
            playing = true;
        } else {
            mp3.close();
            playing = false;
        }
    }
    
    public void playCrashSound() {
        if(playing)
            mp3.playCrash();
    }

}
