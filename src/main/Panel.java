package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Panel extends JPanel {

    private static final long serialVersionUID = 1L;

    public Copter copter;
    private Main main;

    public int movement = 0;
    public int points = 0;
    private int speed = 4;
    public int level1 = 0;
    public int level2 = 0;

    private int[][][] blokkering;

    public Panel(int[][][] blokkering, Main main) {
        this.main = main;
        addMouseListener(main);
        this.blokkering = blokkering;
        setBackground(new Color(56, 25, 25));
        copter = new Copter();
        requestFocus();
    }

    public void draw() {
        movement += speed;
        if ((int) (points / 500) > level1) {
            level1++;
            speed++;
        }
        if ((int) ((250 + points) / 1000) > level2) {
            level2++;
            for (int i = 0; i < blokkering[0].length; i++) {
                blokkering[0][i][0] = (int) (blokkering[0][i][0] * 1.1);
                blokkering[1][i][0] = (int) (blokkering[1][i][0] * 1.1);
            }
        }
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(new Color(100, 200, 100));

        for (int i = 0; i < blokkering[0].length; i++) {

            if (i * 300 - movement >= -30 && i * 300 - movement < 600) {
                /**
                 * Sjekke om man har kr�sjet
                 */
                if (copter.getyPosition() < blokkering[0][i][0] * 10
                        && // den er i h�yde med s�ylen
                        copter.getxPosition() + 20 > i * 300 - movement
                        && copter.getxPosition() < i * 300 - movement + 30) {
                    if (blokkering[0][i][1] == 0) {
                        stop();
                        g.setFont(new Font("Times New Roman", Font.BOLD, 40));
                        g.setColor(new Color(43, 234, 5));
                        g.drawString(points + "", 200, 200);
                        g.setColor(new Color(100, 200, 100));
                        main.playCrashSound();
                    } else {
                        points += 12;
                    }
                    
                }
                /**
                 * Ferdig med sjekkingen
                 */
                if (blokkering[0][i][1] == 0) {
                    g.fillRect(i * 300 - movement, 0, 30,
                            blokkering[0][i][0] * 10);
                } else {
                    g.setColor(new Color(232, 223, 0));
                    g.fillRect(i * 300 - movement, 0, 30,
                            blokkering[0][i][0] * 10);
                    g.setColor(new Color(100, 200, 100));
                }

            }
            if (i * 300 - movement + 150 >= -30
                    && i * 300 - movement + 150 < 600) {
                /**
                 * Sjekke om man har kr�sjet
                 */
                if (copter.getyPosition() + 20 > 400 - blokkering[1][i][0] * 10
                        && // den er i h�yde med s�ylen
                        copter.getxPosition() + 20 > i * 300 - movement + 150
                        && copter.getxPosition() < i * 300 - movement + 150
                                + 30) {
                    if (blokkering[1][i][1] == 0) {
                        stop();
                        g.setFont(new Font("Times New Roman", Font.BOLD, 40));
                        g.setColor(new Color(43, 234, 5));
                        g.drawString(points + "", 200, 200);
                        g.setColor(new Color(100, 200, 100));
                    } else {
                        points += 12;
                    }
                }
                /**
                 * Ferdig med sjekkingen
                 */
                if (blokkering[1][i][1] == 0) {
                    g.fillRect(i * 300 - movement + 150,
                            400 - blokkering[1][i][0] * 10, 30,
                            blokkering[1][i][0] * 10);
                } else {
                    g.setColor(new Color(232, 223, 0));
                    g.fillRect(i * 300 - movement + 150,
                            400 - blokkering[1][i][0] * 10, 30,
                            blokkering[1][i][0] * 10);
                    g.setColor(new Color(100, 200, 100));
                }
            }
        }

        g.setColor(new Color(0, 0, 200));
        g.setFont(new Font("Times New Roman", Font.BOLD, 18));
        g.drawString(points + "", 540, 20);

        g.setColor(new Color(0, 100, 100));
        g.fillRect(copter.getxPosition(), copter.getyPosition(), 20, 20);

        if (copter.falling) {
            if (copter.howLong > 3) {
                copter.howLong = copter.howLong + 1;
            } else {
                copter.howLong = copter.howLong * 1.3;
            }
            copter.setyPosition((int) (copter.getyPosition() + copter.howLong));
        } else {
            if (copter.howLong > 3) {
                copter.howLong = copter.howLong + 1;
            } else {
                copter.howLong = copter.howLong * 1.3;
            }
            copter.setyPosition((int) (copter.getyPosition() - copter.howLong));
        }
    }

    public void setBlokkering(int[][][] blokkering) {
        this.blokkering = blokkering;
    }

    private void stop() {
        speed = 4;
        main.play = false;
    }

    public void tick() {
        points++;
        draw();
    }
}
