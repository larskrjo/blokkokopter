package main;

/*************************************************************************
 *  Compilation:  javac -classpath .:jl1.0.jar MP3.java         (OS X)
 *                javac -classpath .;jl1.0.jar MP3.java         (Windows)
 *  Execution:    java -classpath .:jl1.0.jar MP3 filename.mp3  (OS X / Linux)
 *                java -classpath .;jl1.0.jar MP3 filename.mp3  (Windows)
 *  
 *  Plays an MP3 file using the JLayer MP3 library.
 *
 *  Reference:  http://www.javazoom.net/javalayer/sources.html
 *
 *
 *  To execute, get the file jl1.0.jar from the website above or from
 *
 *      http://www.cs.princeton.edu/introcs/24inout/jl1.0.jar
 *
 *  and put it in your working directory with this file MP3.java.
 *
 *************************************************************************/

import javazoom.jl.player.Player;

public class MP3 {
    private Player player;
    private Player player1;

    // constructor that takes the name of an MP3 file
    public MP3() {
    }

    public void close() {
        if (player != null)
            player.close();
    }

    // play the MP3 file to the sound card
    public void play() {
        try {
            player = new Player(MP3.class.getResourceAsStream("test.mp3"));
        } catch (Exception e) {
            System.out.println(e);
        }

        // run in new thread to play in background
        new Thread() {
            public void run() {
                try {
                    player.play();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }.start();
    }
    
    // play the MP3 file to the sound card
    public void playCrash() {
        try {
            player1 = new Player(MP3.class.getResourceAsStream("crash.mp3"));
        } catch (Exception e) {
            System.out.println(e);
        }

        // run in new thread to play in background
        new Thread() {
            public void run() {
                try {
                    player1.play();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }.start();
    }

}
