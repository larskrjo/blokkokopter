package main;

public class Copter {

    private int xPosition = 200;
    private int yPositon = 250;

    public boolean falling = true;
    public double howLong = 0;

    public Copter() {

    }

    public int getxPosition() {
        return xPosition;
    }

    public int getyPosition() {
        return yPositon;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public void setyPosition(int yPositon) {
        this.yPositon = yPositon;
    }

}
